import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Card } from '../services/CardsService';
import { CardParams, CardsService } from '../services/CardsService';

interface CardsState {
    cards: Card[];
};

const initialState : CardsState = {
    cards: [],
}

export const addCard = createAsyncThunk(
    'cards/addCard',
    async (params: CardParams) => {
        const newCard = await CardsService.addCard(params).catch(
            (error) => { throw error; }
        );
        return newCard;
    }
);

export const getCards = createAsyncThunk(
    'cards/getCards',
    async () => {
        const cards = await CardsService.getCards().catch(
            (error) => { throw error; }
        );
        return cards;
    }
);

export const removeCard = createAsyncThunk(
    'cards/removeCard',
    async (id: number) => {
        const isRemoved = await CardsService.removeCard(id).catch(
            (error) => { throw error; }
        );
        return isRemoved;
    }
);

export const editCard = createAsyncThunk(
    'cards/editCard',
    async (arg : { id: number, params: CardParams }) => {
        const card = await CardsService.editCard(arg.id, arg.params).catch(
            (error) => { throw error; }
        );
        return card;
    }
);

export const cardsSlice = createSlice({
    name: 'cards',
    initialState,
    reducers: {
        orderByTitle: (state, action: PayloadAction<{ desc: boolean }>) => {
            state.cards = state.cards.sort((card1, card2) => {
                if (action.payload.desc) {
                    return card2.title.localeCompare(card1.title);
                }

                return card1.title.localeCompare(card2.title);
            })
        },
        orderByDate: (state, action: PayloadAction<{ desc: boolean }>) => {
            state.cards = state.cards.sort((card1, card2) => {
                if (action.payload.desc) {
                    return new Date(card2.created).getTime() - new Date(card1.created).getTime(); 
                }

                return new Date(card1.created).getTime() - new Date(card2.created).getTime();
            })
        }
    },
    extraReducers: (builder) => {
        builder.addCase(addCard.fulfilled, (state, action) => {
            state.cards.push(action.payload);
        });

        builder.addCase(removeCard.fulfilled, (state, action) => {
            if (action.payload) {
                const id = Math.floor(action.meta.arg);
                state.cards = state.cards.filter((card) => card.id !== id);
            }
        });

        builder.addCase(editCard.fulfilled, (state, action) => {
            if (action.payload) {
                const id = Math.floor(action.meta.arg.id);
                let editedCard = state.cards.find((card) => card.id === id);

                if (editedCard) {
                    const { params } = action.meta.arg;
                    editedCard.title = params.title;
                    editedCard.description = params.description;
                    editedCard.image = params.image;
                }
            }
        })

        builder.addCase(getCards.fulfilled, (state, action) => {
            const cards = action.payload;
            state.cards = cards ? cards : [];
        })
    }
});

export const { orderByTitle, orderByDate } = cardsSlice.actions;

export default cardsSlice.reducer;