import { RootState } from '../store';

const getModuleState = (state: RootState) =>
    state.cards;

export const getCards = (state : RootState) =>
    getModuleState(state).cards;