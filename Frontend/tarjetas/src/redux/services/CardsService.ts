// Types
export type Card = {
    id: number,
    created: string,
    title: string,
    description: string,
    image: string
};

export type CardParams = {
    title: string,
    description: string,
    image: string
}

// ITEM KEY
export const CARDS_ITEM = "cards";

// SERVICE
export class CardsService {
    static addCard = (newCard: CardParams) => new Promise<Card>((resolve, reject) => {
        let cards : Card[] = [];

        const cardStream = localStorage.getItem(CARDS_ITEM);

        if (cardStream != null) {
            try {
                cards = JSON.parse(cardStream) as Card[];
            } catch (error) {
                reject(error);
            }
        }

        const lastIndex = cards.length === 0 ? 0 : cards.reduce((previous, current) => (
            previous.id > current.id ? previous : current
        )).id;

        let card : Card = {
            id: lastIndex + 1,
            created: new Date().toJSON(),
            ...newCard
        }

        cards.push(card);
        localStorage.setItem(CARDS_ITEM, JSON.stringify(cards));

        resolve(card);
    });

    static editCard = (id: number, params: CardParams) => new Promise<Card | undefined>((resolve, reject) => {
        let cards = null;

        const cardStream = localStorage.getItem(CARDS_ITEM);

        if (cardStream != null) {
            try {
                cards = JSON.parse(cardStream) as Card[];
            } catch (error) {
                reject(error);
            }

            if (cards != null) {
                let editCard = cards.find(card => card.id === Math.floor(id));

                if (editCard === undefined) {
                    resolve(undefined);
                } else {
                    editCard.title = params.title;
                    editCard.description = params.description;
                    editCard.image = params.image;

                    localStorage.setItem(CARDS_ITEM, JSON.stringify(cards));

                    resolve(editCard);
                }
            }
        }
    });

    static getCards = () => new Promise<Card[] | null>((resolve, reject) => {
        let cards : Card[] | null = null;

        const cardStream = localStorage.getItem(CARDS_ITEM);

        if (cardStream != null) {
            try {
                cards = JSON.parse(cardStream) as Card[];
            } catch (error) {
                reject(error);
            }
        }

        resolve(cards);
    });

    static removeCard = (id: number) => new Promise<boolean>((resolve, reject) => {
        let cards : Card[] | null = null;

        const cardStream = localStorage.getItem(CARDS_ITEM);

        if (cardStream != null) {
            try {
                cards = JSON.parse(cardStream) as Card[];
            } catch (error) {
                reject(error)
            }

            if (cards != null)
            {
                const existsCard = cards.some((card) => card.id === Math.floor(id));
                const newCards = cards.filter((card) => card.id !== Math.floor(id));
                
                localStorage.setItem(CARDS_ITEM, JSON.stringify(newCards));

                resolve(existsCard);
            }
        }
    });
};