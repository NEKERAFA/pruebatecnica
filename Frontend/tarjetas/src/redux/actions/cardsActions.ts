import * as cardsElements from '../reducers/cardsReducer';

export const { getCards, addCard, editCard, removeCard, orderByTitle, orderByDate } = cardsElements;