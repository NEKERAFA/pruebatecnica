import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import * as cardsActions from '../../redux/actions/cardsActions';
import { AppBar, IconButton, SvgIcon, Toolbar, Typography } from '@material-ui/core';
import { SortByAlpha } from '@material-ui/icons';
import SortByAlphaAsc from '../../shared/icons/SortByAlphaAsc';
import SortByAlphaDesc from '../../shared/icons/SortByAlphaDesc';
import SortByDate from '../../shared/icons/SortByDate';
import SortByDateAsc from '../../shared/icons/SortByDateAsc';
import SortByDateDesc from '../../shared/icons/SortByDateDesc';

type BarProps = {
    title: string
}

const SimpleAppBar: React.FunctionComponent<BarProps> = ({ title }) => {
    // State
    const [descAlpha, setDescAlpha] = useState<boolean | undefined>(undefined);
    const [descDate, setDescDate] = useState<boolean | undefined>(undefined);

    // Store
    const dispatch = useDispatch();

    // Handler
    const onSortByAlpha = () => {
        dispatch(cardsActions.orderByTitle({ desc: descAlpha ? descAlpha : false }));
        setDescAlpha(!descAlpha);
        setDescDate(undefined);
    }

    const onSortByDate = () => {
        dispatch(cardsActions.orderByDate({ desc: descDate ? descDate : false }));
        setDescDate(!descDate);
        setDescAlpha(undefined);
    }

    return <AppBar color="primary" position="static">
        <Toolbar>
            <Typography variant="h6" component="h1">
                {title}
            </Typography>
            <div style={{ flexGrow: 1 }} />
            <div style={{ display: 'flex' }}>
                <IconButton onClick={onSortByAlpha} color="inherit">
                    {descAlpha !== undefined ? <SvgIcon>{descAlpha ? <SortByAlphaDesc />  : <SortByAlphaAsc />}</SvgIcon> : <SortByAlpha />}
                </IconButton>
                <IconButton onClick={onSortByDate} color="inherit">
                    <SvgIcon>{descDate !== undefined ? (descDate ? <SortByDateDesc /> : <SortByDateAsc />) : <SortByDate />}</SvgIcon>
                </IconButton>
            </div>
        </Toolbar>
    </AppBar>
};

export default SimpleAppBar;