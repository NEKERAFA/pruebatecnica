import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Typography } from '@material-ui/core';
import React from 'react';
import { useTranslation } from 'react-i18next';

// Types
type DialogProps = {
    open: boolean,
    title: string,
    message: string,
    onClose: () => void,
    onAccept: () => void
}

const SimpleDialog : React.FunctionComponent<DialogProps> = (props: DialogProps) => {
    // i18n
    const { t } = useTranslation("App");

    return <Dialog 
        open={props.open}
        onClose={props.onClose}
        aria-labelledby="dialog-title"
        aria-describedby="dialog-description"
        fullWidth
    >
        <DialogTitle>
            <Typography id="dialog-title" variant="h6" component="h2">{props.title}</Typography>
        </DialogTitle>
        <DialogContent>
            <DialogContentText id="dialog-description">
                {props.message}
            </DialogContentText>
        </DialogContent>
        <DialogActions>
            <Button onClick={props.onAccept} variant="contained" color="primary" disableElevation>
                {t("Accept")}
            </Button>
            <Button onClick={props.onAccept} variant="outlined" color="secondary" disableElevation autoFocus>
                {t("Close")}
            </Button>
        </DialogActions>
    </Dialog>
};

export default SimpleDialog;