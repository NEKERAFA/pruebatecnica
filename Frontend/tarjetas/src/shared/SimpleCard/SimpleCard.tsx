import React from 'react';
import { Card, CardContent, CardMedia, CardActions, IconButton, Typography } from '@material-ui/core';
import useStyle from './SimpleCardStyles';
import { Delete, Edit } from '@material-ui/icons';
import { useTranslation } from 'react-i18next';

type CardProps = {
    title: string,
    description: string,
    image: string,
    onEdit: () => void,
    onRemove: () => void,
}

const SimpleCard : React.FunctionComponent<CardProps> = (props: CardProps) => {
    // i18n
    const { t } = useTranslation("App");

    // Styles
    const classes = useStyle({});

    return <Card>
        <CardMedia
            className={classes.media}
            image={props.image}
            title={props.title}
        >
            <Typography className={classes.title} variant="h6" component="h2">
                {props.title}
            </Typography>
        </CardMedia>
        <CardContent>
            <Typography variant="body1" component="p">
                {props.description}
            </Typography>
        </CardContent>
        <CardActions disableSpacing>
            <IconButton onClick={props.onEdit} aria-label={t("Edit")}>
                <Edit />
            </IconButton>
            <IconButton onClick={props.onRemove} aria-label={t("Delete")}>
                <Delete />
            </IconButton>
      </CardActions>
    </Card>
}

export default SimpleCard;