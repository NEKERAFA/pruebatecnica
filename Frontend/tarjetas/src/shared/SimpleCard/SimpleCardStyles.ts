import { makeStyles, Theme } from '@material-ui/core/styles';
import { BaseCSSProperties } from '@material-ui/core/styles/withStyles';

interface CardStyleProps {
    media?: BaseCSSProperties;
    title?: BaseCSSProperties;
}

const useStyle = makeStyles<Theme, CardStyleProps>(theme => ({
    media: {
        height: 160,
        position: "relative",
    },
    title: {
        color: "#fff",
        position: "absolute",
        bottom: theme.spacing(2),
        left: theme.spacing(2),
    }
}));

export default useStyle;