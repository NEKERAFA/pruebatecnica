import React from 'react';

const SortByAlphaAsc : React.FunctionComponent = () => (
    <path d="M 14.94,4.66 H 10.22 L 12.58,2.3 Z M 6.1,6.27 1.6,17.73 h 1.84 l 0.92,-2.45 h 5.11 l 0.92,2.45 h 1.84 L 7.74,6.27 Z m -1.13,7.37 1.94,-5.18 1.94,5.18 z m 10.76,2.5 h 6.12 v 1.59 h -8.53 v -1.29 l 5.92,-8.56 h -5.88 v -1.6 h 8.3 v 1.26 z"/>
);

export default SortByAlphaAsc;