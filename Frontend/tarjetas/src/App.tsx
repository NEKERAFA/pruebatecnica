import React from 'react';
import { useTranslation } from 'react-i18next';
import AppBar from './shared/SimpleAppBar/SimpleAppBar';
import Cards from './components/Cards/Cards';

const App = () => {
    const { t } = useTranslation('App');
    
    return <React.Fragment>
        <AppBar title={t("Cards")} />
        <Cards />
    </React.Fragment>;
}

export default App;
