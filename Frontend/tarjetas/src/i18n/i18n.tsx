import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import app_es from './es/App.json';

const resources = {
    es: {
        App: app_es
    }
}

i18n.use(initReactI18next)
    .init({
        lng: 'es',
        resources,
        keySeparator: false,
        interpolation: {
            escapeValue: false
        }
    });

export default i18n;
