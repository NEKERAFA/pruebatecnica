import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Fab, Grid } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import CardItem from '../../shared/SimpleCard/SimpleCard';
import useStyles from './CardsStyles';
import * as cardsActions from '../../redux/actions/cardsActions';
import * as cardsSelector from '../../redux/selectors/cardsSelector';
import CardDialog from './CardDialog/CardDialog';
import { useTranslation } from 'react-i18next';
import { Card, CardParams } from '../../redux/services/CardsService';
import NoticeDialog from '../../shared/SimpleDialog/SimpleDialog';

const Cards: React.FunctionComponent = () => {
    // i18n
    const { t } = useTranslation('App');

    // Styles
    const classes = useStyles({});

    // Store
    const dispatch = useDispatch();
    const cards = useSelector(cardsSelector.getCards);

    // States
    const [cardDialog, setCardDialog] = useState(false);
    const [removeDialog, setRemoveDialog] = useState(false);
    const [selectedCard, setSelectedCard] = useState<Card | undefined>(undefined);

    // Effects
    useEffect(() => {
        dispatch(cardsActions.getCards());
    }, []);

    useEffect(() => {
        if (cardDialog) setCardDialog(false);
        if (removeDialog) setRemoveDialog(false);
    }, [cards]);

    // Handlers
    const onOpenDialog = () => {
        setCardDialog(true);
    }

    const onCloseCardDialog = () => {
        setCardDialog(false);
    }

    const onCloseRemoveDialog = () => {
        setSelectedCard(undefined);
        setRemoveDialog(false);
    }

    const onSubmitCardDialog = (form: CardParams, id: number | undefined) => {
        if (id) {
            dispatch(cardsActions.editCard({ id, params: form }))
        } else {
            dispatch(cardsActions.addCard(form));
        }
    }

    const onRemoveCard = (card: Card) => () => {
        setSelectedCard(card);
        setRemoveDialog(true);
    }

    const onSubmitRemoveDialog = () => {
        if (selectedCard !== undefined) {
            dispatch(cardsActions.removeCard(selectedCard.id));
        } else {
            setRemoveDialog(false);
        }

        setSelectedCard(undefined);
    }

    const onEditCard = (card: Card) => () => {
        setSelectedCard(card);
        setCardDialog(true);
    }

    return <div className={classes.root}>
        <Grid container alignItems="center" justify="center" spacing={4}>
            {cards.map((cardInfo, index) => <Grid key={index} item xs={12} sm={6} md={3} >
                <CardItem
                    title={cardInfo.title}
                    description={cardInfo.description}
                    image={cardInfo.image}
                    onEdit={onEditCard(cardInfo)}
                    onRemove={onRemoveCard(cardInfo)}
                />
            </Grid>)}
        </Grid>
        <CardDialog card={selectedCard} open={cardDialog} onClose={onCloseCardDialog} onSubmit={onSubmitCardDialog} />
        <NoticeDialog open={removeDialog} onClose={onCloseRemoveDialog} onAccept={onSubmitRemoveDialog} title={t("Delete card")} message={t("Do you want to delete the card?")} />
        <Fab onClick={onOpenDialog} className={classes.fab} color="secondary" aria-label="add">
            <AddIcon />
        </Fab>
    </div>
}

export default Cards;