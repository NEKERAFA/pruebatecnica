import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Grid, TextField, Typography } from '@material-ui/core';
import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { Card, CardParams } from '../../../redux/services/CardsService';
import * as Utils from '../../../shared/Utils';

type CardDialogProps = {
    open: boolean,
    onClose: () => void,
    onSubmit: (form : CardParams, id : number | undefined) => void,
    card?: Card
}

const CardDialog = (props: CardDialogProps) => {
    // i18n
    const { t } = useTranslation('App');

    // States
    const [open, setOpen] = useState(false);
    const [title, setTitle] = useState("");
    const [titleError, setTitleError] = useState(false);
    const [description, setDescription] = useState("");
    const [descError, setDescError] = useState(false);
    const [image, setImage] = useState("");
    const [imageError, setImageError] = useState(false);

    // Effects
    useEffect(() => {
        setOpen(props.open);
        if (title !== "") setTitle("");
        if (description !== "") setDescription("");
        if (image !== "") setImage("");
    }, [props.open]);

    useEffect(() => {
        if (props.card !== undefined) {
            setTitle(props.card.title);
            setDescription(props.card.description);
            setImage(props.card.image);
        }
    }, [props.card]);

    // Handlers
    const onSubmit = () => {
        const titleStr = title.trim();
        const descStr = description.trim();
        const imageStr = image.trim();

        let error = false;

        if (titleStr === "") {
            setTitleError(true);
            error = true;
        }

        if (descStr === "") {
            setDescError(true);
            error = true;
        }

        if (imageStr === "" || !Utils.validURL(imageStr)) {
            setImageError(true);
            error = true;
        }
        
        if (!error) {
            props.onSubmit({ title: titleStr, description: descStr, image: imageStr }, props.card ? props.card.id : undefined);
        }
    }

    return <Dialog 
        open={open} 
        onClose={props.onClose} 
        aria-labelledby="card-dialog"
        fullWidth
    >
        <DialogTitle id="card-dialog">
            <Typography variant="h6" component="h2">{props.card ? t("Edit card") : t("Add card")}</Typography>
        </DialogTitle>
        <DialogContent>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <TextField 
                        label={t("Title")}
                        value={title}
                        onChange={e => {
                            setTitle(e.target.value);
                            setTitleError(false);
                        }}
                        error={titleError}
                        helperText={titleError ? t("Title required") : ""}
                        focused
                        fullWidth
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField 
                        label={t("Description")}
                        value={description}
                        onChange={e => {
                            setDescription(e.target.value);
                            setDescError(false);
                        }}
                        error={descError}
                        helperText={descError ? t("Description required") : ""}
                        rows={4}
                        fullWidth
                        multiline
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        type="url"
                        label={t("Image (URL)")}
                        error={imageError}
                        helperText={imageError ? t("URL required") : ""}
                        value={image}
                        onChange={e => {
                            setImage(e.target.value);
                            setImageError(false);
                        }}
                        fullWidth
                    />
                </Grid>
            </Grid>
        </DialogContent>
        <DialogActions>
            <Button onClick={() => onSubmit()} variant="contained" color="primary" disableElevation>
                {t("Add")}
            </Button>
            <Button onClick={props.onClose} variant="outlined" color="secondary" disableElevation autoFocus>
                {t("Close")}
            </Button>
        </DialogActions>
    </Dialog>
};

export default CardDialog;