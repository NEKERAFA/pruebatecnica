import { makeStyles, Theme } from '@material-ui/core/styles';
import { BaseCSSProperties } from '@material-ui/core/styles/withStyles';

interface CardsStyleProps {
    root?: BaseCSSProperties;
}

const useStyle = makeStyles<Theme, CardsStyleProps>(theme => ({
    root: {
        margin: theme.spacing(4)
    },
    fab: {
        position: "fixed",
        bottom: theme.spacing(2),
        right: theme.spacing(2),
    }
}));

export default useStyle;