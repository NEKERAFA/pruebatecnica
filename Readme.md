# Prueba técnica

## Backend

El proyecto está hecho en .NET Core 3.1. Para ejecutarlo hay dos formas, o como intérprete o leyendo de un fichero:

+ **Intérprete**: sin parámetros, el fin de la entrada se detecta como una línea en blanco.

```bash
$ dotnet run
```

+ **Fichero**: ruta como primer parámetro.

```bash
$ dotnet run <PATH>
```

## Frontend

El proyecto está hecho con nodejs 10.19.0. Para ejecutarlo se necesita npm:

```bash
$ npm start
```
