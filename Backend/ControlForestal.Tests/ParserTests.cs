using Microsoft.VisualStudio.TestTools.UnitTesting;
using ParqueMonserrat.ControlForestal.Ast;
using ParqueMonserrat.ControlForestal.Lexer;
using System.Collections.Generic;

namespace ParqueMonserrat.ControlForestal.Tests
{
    [TestClass]
    public class ParserTests
    {
        [TestMethod]
        public void Tokenize_CheckParsing()
        {
            var line = 1;
            var character = 0;

            var tokens = new List<IToken>
            {
                new Number(++character, line, 5),
                new Space(++character, line),
                new Number(++character, line, 6),
                new NewLine(++character, line++),
                new Number(character = 1, line, 3),
                new Space(++character, line),
                new Number(++character, line, 4),
                new Space(++character, line),
                new Letter(++character, line, 'E'),
                new NewLine(++character, line++),
                new Letter(character = 1, line, 'L')
            };

            var ast = Parser.Parse(tokens);

            Assert.AreEqual(6u, ast.Dimensions.Rows);
            Assert.AreEqual(5u, ast.Dimensions.Columns);
            Assert.AreEqual(1, ast.Drones.Count);
            Assert.AreEqual(4u, ast.Drones[0].Row);
            Assert.AreEqual(3u, ast.Drones[0].Column);
            Assert.AreEqual(DronItem.DirectionType.East, ast.Drones[0].PointAt);
            Assert.AreEqual(1, ast.Drones[0].Actions.Count);
            Assert.IsInstanceOfType(ast.Drones[0].Actions[0], typeof(RotateLeftAction));
        }

        [TestMethod]
        public void Tokenize_NotToken()
        {
            var tokens = new List<IToken>();
            Assert.ThrowsException<TokenizeException>(() => Parser.Parse(tokens));
        }
    }
}