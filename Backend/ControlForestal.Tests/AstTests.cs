using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ParqueMonserrat.ControlForestal.Ast;
using ParqueMonserrat.ControlForestal.Lexer;
using ParqueMonserrat.ControlForestal;
using System.Collections.Generic;

namespace ParqueMonserrat.ControlForestal.Tests
{
    [TestClass]
    public class AstTest
    {
        [TestMethod]
        public void CheckExecution()
        {
            // Input con el ejemplo
            var stringExample = new StringBuilder();
            stringExample.AppendLine("5 5");
            stringExample.AppendLine("3 3 E");
            stringExample.AppendLine("L");
            stringExample.AppendLine("3 3 E");
            stringExample.AppendLine("MMRMMRMRRM");
            stringExample.AppendLine("1 2 N");
            stringExample.AppendLine("LMLMLMLMMLMLMLMLMM");

            using (var stream = new StringReader(stringExample.ToString()))
            {
                // Tokenizo el input
                var tokens = Tokenizer.Tokenize(stream);

                // Parseo los tokens
                var ast = Parser.Parse(tokens);

                // Ejecuto el input
                var context = new Context();
                ast.Execute(context);

                // Compruebo la ejecución
                Assert.AreEqual(5, context.Rows);
                Assert.AreEqual(5, context.Columns);
                Assert.AreEqual(3, context.Drones.Count);
                Assert.AreEqual(3, context.Drones[0].Row);
                Assert.AreEqual(3, context.Drones[0].Column);
                Assert.AreEqual(Dron.Direction.North, context.Drones[0].PointAt);
                Assert.AreEqual(1, context.Drones[1].Row);
                Assert.AreEqual(5, context.Drones[1].Column);
                Assert.AreEqual(Dron.Direction.East, context.Drones[1].PointAt);
                Assert.AreEqual(4, context.Drones[2].Row);
                Assert.AreEqual(1, context.Drones[2].Column);
                Assert.AreEqual(Dron.Direction.North, context.Drones[2].PointAt);
            }
        }
    }
}