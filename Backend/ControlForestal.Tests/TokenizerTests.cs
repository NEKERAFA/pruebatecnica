using Microsoft.VisualStudio.TestTools.UnitTesting;
using ParqueMonserrat.ControlForestal.Lexer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ParqueMonserrat.ControlForestal.Tests
{
    [TestClass]
    public class TokenizerTests
    {
        [TestMethod]
        public void Tokenize_CheckParsing()
        {
            // Creamos la entrada
            var input = new StringBuilder();
            input.AppendLine("1 10");
            input.AppendLine("-10NL");
            input.AppendLine("$1/F");

            // Creamos la salida esperada
            var expected = new List<IToken>
            {
                new Number(1, 1, 1),
                new Space(2, 1),
                new Number(3, 1, 10),
                new NewLine(5, 1),
                new Symbol(1, 2, '-'),
                new Number(2, 2, 10),
                new Letter(4, 2, 'N'),
                new Letter(5, 2, 'L'),
                new NewLine(6, 2),
                new Symbol(1, 3, '$'),
                new Number(2, 3, 1),
                new Symbol(3, 3, '/'),
                new Letter(4, 3, 'F'),
                new NewLine(5, 3)
            };

            using (var stream = new StringReader(input.ToString()))
            {
                List<IToken> tokens = Tokenizer.Tokenize(stream);

                CollectionAssert.AreEqual(expected, tokens);
            }
        }

        [TestMethod]
        public void Tokenize_CheckNumberOverflow()
        {
            using (var stream = new StringReader("123456789012345"))
            {
                Assert.ThrowsException<TokenizeException>(() => Tokenizer.Tokenize(stream));
            }
        }
    }
}
