using System.Collections.Generic;

namespace ParqueMonserrat.ControlForestal
{
    /// <summary>
    /// Representa el contexto actual del programa.
    /// Es un objeto abierto ya que se va a manipular por el AST
    /// </summary>
    public class Context
    {
        /// Tamaño de la cuadrícula
        public int Rows { get; set; }
        public int Columns { get; set; }

        /// Drones
        public List<Dron> Drones { get; set; }

        public Context()
        {
            Drones = new List<Dron>();
        }
    }
}