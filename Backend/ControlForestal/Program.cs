﻿using ParqueMonserrat.ControlForestal.Lexer;
using System.IO;
using System;
using System.Text;

namespace ParqueMonserrat.ControlForestal
{
    public class Program
    {
        public static void Main(string[] args)
        {
            TextReader stream = null;

            try
            {
                if (args.Length > 0)
                {
                    // Si se pasan parámetros, se coge el primero como ruta a un fichero
                    stream = File.OpenText(args[0]);
                }
                else 
                {
                    // Si no, se lee desde la consola
                    stream = GetInput();
                }

                // Se tokeniza la entrada
                var tokens = Tokenizer.Tokenize(stream);

                // Se parsea los tokens
                var ast = Parser.Parse(tokens);

                // Se ejecuta el fichero
                var context = new Context();
                ast.Execute(context);

                // Se pinta donde terminan los drones
                foreach (var dron in context.Drones)
                {
                    PrintDron(dron);
                }
            }
            catch (TokenizeException ex)
            {
                Console.WriteLine($"error: {ex.Character}, {ex.Line}: {ex.Message}");
                Console.WriteLine("Press any key to close the console");
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"error: {ex.Message}");
                Console.WriteLine(ex.StackTrace);
                Console.WriteLine("Press any key to close the console");
                Console.ReadKey();
            }
            finally
            {
                if (stream != null)
                    stream.Dispose();
            }
        }

        private static StringReader GetInput()
        {
            Console.Write("> ");

            var input = new StringBuilder();

            // Leemos el input
            var reading = true;
            while (reading)
            {
                var line = Console.ReadLine();

                // Se comprueba que la linea no sea una linea en blanco
                if (string.IsNullOrWhiteSpace(line))
                {
                    reading = false;
                }
                else
                {
                    input.AppendLine(line);
                    Console.Write("> ");
                }
            }

            return new StringReader(input.ToString());
        }

        private static void PrintDron(Dron dron)
        {
            // Se pinta la posición
            Console.Write($"{dron.Column} {dron.Row} ");

            // Obtenemos la dirección
            var pointAt = ' ';
            switch (dron.PointAt)
            {
                case Dron.Direction.North:
                    pointAt = 'N';
                    break;
                case Dron.Direction.South:
                    pointAt = 'S';
                    break;
                case Dron.Direction.West:
                    pointAt = 'W';
                    break;
                case Dron.Direction.East:
                    pointAt = 'E';
                    break;
            }

            // Se pinta la dirección
            Console.WriteLine(pointAt);
        }
    }
}
