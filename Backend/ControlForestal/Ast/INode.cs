namespace ParqueMonserrat.ControlForestal.Ast
{
    /// <summary>
    /// Representa un nodo del árbol abstracto sintáctico.
    /// </summary>
    public interface INode
    {
        void Execute(Context context);
    }
}