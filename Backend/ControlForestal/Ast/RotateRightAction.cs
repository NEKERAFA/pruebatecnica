namespace ParqueMonserrat.ControlForestal.Ast
{
    /// <summary>
    /// Item que simboliza rotar un dron a la derecha
    /// </summary>
    public class RotateRightAction : ActionItem
    {
        public override void Execute(Context context)
        {
            // Obtenemos el último dron insertado
            var dron = context.Drones[context.Drones.Count - 1];

            switch (dron.PointAt)
            {
                case Dron.Direction.North:
                    dron.PointAt = Dron.Direction.East;
                    break;
                case Dron.Direction.South:
                    dron.PointAt = Dron.Direction.West;
                    break;
                case Dron.Direction.West:
                    dron.PointAt = Dron.Direction.North;
                    break;
                case Dron.Direction.East:
                    dron.PointAt = Dron.Direction.South;
                    break;
            }
        }

        public override bool IsRotateRightAction()
        {
            return true;
        }
    }
}