using System;
namespace ParqueMonserrat.ControlForestal.Ast
{
    /// <summary>
    /// Item que simboliza la definición de la grid
    /// </summary>
    public class GridItem : INode
    {
        public uint Rows { get; }

        public uint Columns { get; }

        public GridItem(uint rows, uint columns)
        {
            Rows = rows;
            Columns = columns;
        }

        public void Execute(Context context)
        {
            context.Rows = Convert.ToInt32(Rows);
            context.Columns = Convert.ToInt32(Columns);
        }
        
        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is GridItem))
                return false;
            
            var other = obj as GridItem;

            return this.Rows.Equals(other.Rows)
                && this.Columns.Equals(other.Columns);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Rows, Columns);
        }
    }
}