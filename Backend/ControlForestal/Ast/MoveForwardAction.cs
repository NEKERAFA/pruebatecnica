using System;
namespace ParqueMonserrat.ControlForestal.Ast
{
    /// <summary>
    /// Item que simboliza mover un dron hacia delante
    /// </summary>
    public class MoveForwardAction : ActionItem
    {
        public override void Execute(Context context)
        {
            // Obtenemos el último dron insertado
            var dron = context.Drones[context.Drones.Count - 1];

            switch (dron.PointAt)
            {
                case Dron.Direction.North:
                    if (dron.Row == context.Rows)
                        throw new IndexOutOfRangeException($"{dron} is moving out of bound");

                    dron.Row++;

                    break;
                case Dron.Direction.South:
                    if (dron.Row == 0)
                        throw new IndexOutOfRangeException($"{dron} is moving out of bound");
                
                    dron.Row--;

                    break;
                case Dron.Direction.West:
                    if (dron.Column == 0)
                        throw new IndexOutOfRangeException($"{dron} is moving out of bound");

                    dron.Column--;
                    
                    break;
                case Dron.Direction.East:
                    if (dron.Column == context.Columns)
                        throw new IndexOutOfRangeException($"{dron} is moving out of bound");
                    
                    dron.Column++;

                    break;
            }
        }

        public override bool IsRotateRightAction()
        {
            return true;
        }
    }
}