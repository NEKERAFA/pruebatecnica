using System.Text;
using System;
using System.Collections.Generic;

namespace ParqueMonserrat.ControlForestal.Ast
{
    /// <summary>
    /// Item que simboliza un dron
    /// </summary>
    public class DronItem : INode
    {
        public enum DirectionType { North, South, East, West }

        private List<ActionItem> m_actions;

        public uint Row { get; }

        public uint Column { get; }

        public DirectionType PointAt { get; }

        public IList<ActionItem> Actions => m_actions.AsReadOnly();

        public DronItem(uint row, uint column, DirectionType pointAt, List<ActionItem> actions)
        {
            Row = row;
            Column = column;
            PointAt = pointAt;
            m_actions = actions;
        }

        public void Execute(Context context)
        {
            if (Row > context.Rows)
                throw new IndexOutOfRangeException($"{this} row is out of bounds");

            if (Column > context.Columns)
                throw new IndexOutOfRangeException($"{this} column is out of bounds");

            context.Drones.Add(new Dron
            {
                Row = Convert.ToInt32(Row),
                Column = Convert.ToInt32(Column),
                PointAt = toDirection(PointAt)
            });

            foreach (var action in m_actions)
            {
                action.Execute(context);
            }
        }

        private Dron.Direction toDirection(DronItem.DirectionType type)
        {
            var direction = Dron.Direction.North;

            switch (type)
            {
                case DronItem.DirectionType.South:
                    direction = Dron.Direction.South;
                    break;
                case DronItem.DirectionType.East:
                    direction = Dron.Direction.East;
                    break;
                case DronItem.DirectionType.West:
                    direction = Dron.Direction.West;
                    break;
            }

            return direction;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is DronItem))
                return false;
            
            var other = obj as DronItem;

            return this.Row.Equals(other.Row)
                && this.Column.Equals(other.Column)
                && this.PointAt.Equals(other.PointAt)
                && this.m_actions.Equals(other.m_actions);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Row, Column, PointAt, m_actions);
        }

        public override string ToString()
        {
            return $"Dron<{Row}, {Column}, {PointAt}>";
        }
    }
}