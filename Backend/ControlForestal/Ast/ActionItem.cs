namespace ParqueMonserrat.ControlForestal.Ast
{
    /// <summary>
    /// Item que simboliza la acción de un dron mediante un patrón estrategia
    /// </summary>
    public abstract class ActionItem : INode
    {
        public abstract void Execute(Context context);

        public virtual bool IsRotateLeftAction()
        {
            return false;
        }

        public virtual bool IsRotateRightAction()
        {
            return false;
        }

        public virtual bool IsMoveForwardAction()
        {
            return false;
        }
    }
}