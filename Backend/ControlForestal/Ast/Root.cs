using System;
using System.Collections.Generic;

namespace ParqueMonserrat.ControlForestal.Ast
{
    /// <summary>
    /// Raiz de nuestro árbol sintáctico
    /// </summary>
    public class Root : INode
    {
        public List<DronItem> m_drones;

        public GridItem Dimensions { get; }

        public IList<DronItem> Drones => m_drones?.AsReadOnly();

        public Root(GridItem dimensions, List<DronItem> drones)
        {
            Dimensions = dimensions;
            m_drones = drones;
        }

        public void Execute(Context context)
        {
            Dimensions.Execute(context);

            foreach (var dron in m_drones)
            {
                dron.Execute(context);
            }
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Root))
                return false;
            
            var other = obj as Root;

            Console.WriteLine(this.Dimensions.Equals(other.Dimensions));

            return this.Dimensions.Equals(other.Dimensions)
                && this.Drones.Equals(other.Drones);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Dimensions, Drones);
        }
    }
}