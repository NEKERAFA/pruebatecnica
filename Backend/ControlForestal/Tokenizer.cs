using System;
using ParqueMonserrat.ControlForestal.Lexer;
using System.Collections.Generic;
using System.IO;

namespace ParqueMonserrat.ControlForestal
{
    /// <summary>
    /// Clase que se encarga de tokenizar una entrada
    /// </summary>
    public static class Tokenizer
    {
        public static List<IToken> Tokenize(TextReader input)
        {
            var tokens = new List<IToken>();
            var position = 1;
            var line = 1;

            while (input.Peek() != -1)
            {
                var character = Convert.ToChar(input.Read());

                // Comprueba si es un dígito
                if (char.IsDigit(character))
                {
                    AddDigit(character, ref position, line, ref tokens);
                }
                // Comprueba si es una letra
                else if (char.IsLetter(character))
                {
                    AddLetter(character, ref position, line, ref tokens);
                }
                // Comprueba si es un salto de linea Unix-Like
                else if (character == '\n')
                {
                    AddNewLine(ref position, ref line, ref tokens);
                }
                // Comprueba si es un salto de linea Windows-Like
                else if (character == '\r')
                {
                    // Windows-Like
                    if (input.Peek() != -1)
                    {
                        var next = Convert.ToChar(input.Peek());
                        if (next == '\n')
                        {
                            input.Read();
                            AddNewLine(ref position, ref line, ref tokens);
                        }
                        else
                        {
                            AddSymbol(character, ref position, line, ref tokens);
                        }
                    }
                }
                // Comprueba si es un espacio
                else if (char.IsWhiteSpace(character))
                {
                    AddSpace(ref position, line, ref tokens);
                }
                // Añade el caracter como símbolo
                else
                {
                    AddSymbol(character, ref position, line, ref tokens);
                }
            }

            return tokens;
        }

        /// <summary>
        /// Añade o modifica un número a la lista de tokens
        /// </summary>
        private static void AddDigit(char character, ref int position, int line, ref List<IToken> tokens)
        {
            var length = tokens.Count;
            var value = Convert.ToSByte(char.GetNumericValue(character));

            // Añade un nuevo token si es el primero o el último token añadido no es un número
            if (length == 0 || !tokens[length - 1].IsNumber())
            {
                var newToken = new Number(position, line, value);
                tokens.Add(newToken);
            }
            else
            {
                // Añade un digito al último token y comprueba si hay overflow
                var lastToken = (Number) tokens[length - 1];

                try
                {
                    lastToken.AddDigit(value);
                }
                catch (OverflowException)
                {
                    throw new TokenizeException(position, line, "numbers must be in integer range");
                }
            }

            position++;
        }

        /// <summary>
        /// Añade una letra a la lista de tokens
        /// </summary>
        private static void AddLetter(char character, ref int position, int line, ref List<IToken> tokens)
        {
            var newToken = new Letter(position, line, character);
            tokens.Add(newToken);
            position++;
        }

        /// <summary>
        /// Añade un espacio a la lista de tokens
        /// </summary>
        private static void AddSpace(ref int position, int line, ref List<IToken> tokens)
        {
            var newToken = new Space(position, line);
            tokens.Add(newToken);
            position++;
        }

        //// <summary>
        /// Añade un símbolo a la lista de tokens
        /// </summary>
        private static void AddSymbol(char character, ref int position, int line, ref List<IToken> tokens)
        {
            var newToken = new Symbol(position, line, character);
            tokens.Add(newToken);
            position++;
        }

        /// <summary>
        /// Añade una linea nueva a la lista de tokens
        /// </summary>
        private static void AddNewLine(ref int position, ref int line, ref List<IToken> tokens)
        {
            var newToken = new NewLine(position, line);
            tokens.Add(newToken);
            position = 1;
            line++;
        }
    }
}