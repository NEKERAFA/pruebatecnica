using System;
namespace ParqueMonserrat.ControlForestal.Lexer
{
    /// <summary>
    /// Representa un salto de línea.
    /// </summary>
    public class NewLine : Token
    {   
        public NewLine(int position, int line) : base(position, line) { }
        
        public override bool IsNewLine()
        {
            return true;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is NewLine))
                return false;

            var other = obj as NewLine;
            return this.Character == other.Character &&
                this.Line == other.Line;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Character, Line);
        }

        public override string ToString()
        {
            return TypeToString();
        }

        public static string TypeToString()
        {
            return "NewLine";
        }
    }
}