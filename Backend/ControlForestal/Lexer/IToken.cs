namespace ParqueMonserrat.ControlForestal.Lexer
{
    /// <summary>
    /// Define la interfaz token y el método para comprobar el tipo de los tokens
    /// </summary>
    public interface IToken
    {
        int Character { get; }
        int Line { get; }

        bool IsLetter();
        bool IsNumber();
        bool IsSpace();
        bool IsNewLine();
        bool IsSymbol();
    }
}