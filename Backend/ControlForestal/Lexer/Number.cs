using System;

namespace ParqueMonserrat.ControlForestal.Lexer
{
    /// <summary>
    /// Representa un entero positivo.
    /// </summary>
    public class Number : Token
    {
        private uint m_value;

        public uint Value => m_value;

        public Number(int character, int line, sbyte value) : base(character, line)
        {
            m_value = Convert.ToUInt32(value);
        }
        
        public override bool IsNumber()
        {
            return true;
        }

        public void AddDigit(sbyte value)
        {
            checked
            {
                m_value = m_value * 10 + Convert.ToUInt32(value);
            }
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Number))
                return false;

            var other = obj as Number;

            return this.Character == other.Character &&
                this.Line == other.Line &&
                this.Value == other.Value;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Character, Line, Value);
        }

        public override string ToString()
        {
            return $"Number<{Value}>";
        }

        public static string TypeToString()
        {
            return "Number";
        }
    }
}