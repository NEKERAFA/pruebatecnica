namespace ParqueMonserrat.ControlForestal.Lexer
{
    /// <summary>
    /// Define la clase token y los métodos para preguntar por el tipo
    /// </summary>
    public abstract class Token : IToken
    {
        public int Character { get; }
        public int Line { get; }

        public Token(int character, int line)
        {
            Character = character;
            Line = line;
        }

        public virtual bool IsLetter()
        {
            return false;
        }
        
        public virtual bool IsNumber()
        {
            return false;
        }

        public virtual bool IsSpace()
        {
            return false;
        }
        
        public virtual bool IsNewLine()
        {
            return false; 
        }
        
        public virtual bool IsSymbol()
        {
            return false;
        }
    }
}