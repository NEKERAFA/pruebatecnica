using System;
namespace ParqueMonserrat.ControlForestal.Lexer
{
    /// <summary>
    /// Representa un caracter
    /// </summary>
    public class Letter : Token
    {
        public char Value { get; }

        public Letter(int position, int line, char letter) : base(position, line)
        {
            Value = letter;
        }
        
        public override bool IsLetter()
        {
            return true;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Letter))
                return false;

            var other = obj as Letter;

            return this.Character == other.Character &&
                this.Line == other.Line &&
                this.Value == other.Value;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Character, Line, Value);
        }

        public override string ToString()
        {
            return $"{Value}";
        }

        public static string TypeToString()
        {
            return "Letter";
        }
    }
}