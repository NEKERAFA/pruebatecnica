using System;
namespace ParqueMonserrat.ControlForestal.Lexer
{
    /// <summary>
    /// Representa cualquier cosa que no sea un entero positivo, un caracter, un espacio o un salto de linea
    /// </summary>
    public class Symbol : Token
    {
        public char Value { get; }

        public Symbol(int character, int line, char symbol) : base(character, line) 
        { 
            Value = symbol;
        }
        
        public override bool IsSymbol()
        {
            return true;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Symbol))
                return false;

            var other = obj as Symbol;

            return this.Character == other.Character &&
                this.Line == other.Line &&
                this.Value == other.Value;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Character, Line, Value);
        }

        public override string ToString()
        {
            return $"Symbol<{Value}>";
        }

        public static string TypeToString()
        {
            return "Symbol";
        }
    }
}