using System;

namespace ParqueMonserrat.ControlForestal.Lexer
{
    /// <summary>
    /// Representa un espacio en blanco
    /// </summary>
    public class Space : Token
    {   
        public Space(int position, int line) : base(position, line) { }

        public override bool IsSpace()
        {
            return true;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Space))
                return false;

            var other = obj as Space;

            return this.Character == other.Character &&
                this.Line == other.Line;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Character, Line);
        }

        public override string ToString()
        {
            return TypeToString();
        }

        public static string TypeToString()
        {
            return "Space";
        }
    }
}