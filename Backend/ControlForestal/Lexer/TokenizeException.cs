using System;
namespace ParqueMonserrat.ControlForestal.Lexer
{
    public class TokenizeException : Exception
    {
        public int Character { get; }
        public int Line { get; }

        public TokenizeException(int character, int line, string message) : base(message)
        {
            Character = character;
            Line = line;
        }
    }
}