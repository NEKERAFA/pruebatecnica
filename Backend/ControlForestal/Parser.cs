using ParqueMonserrat.ControlForestal.Lexer;
using ParqueMonserrat.ControlForestal.Ast;
using System.Collections.Generic;

namespace ParqueMonserrat.ControlForestal
{
    /// <summary>
    /// Clase que se encarga de convertir una lista de tokens en un árbol sintáctico, el cual luego se puede ejecutar
    /// con un contexto dado.
    /// </sumary>
    public static class Parser
    {
        /// <summary>
        /// Crea el árbol sintáctico
        /// </sumary>
        public static Root Parse(List<IToken> tokens)
        {
            if (tokens.Count == 0)
                throw new TokenizeException(1, 1, "unexpected EOF");

            var position = 0;
            var grid = GetGrid(ref position, ref tokens);
            var drones = GetDrones(ref position, ref tokens);

            if (drones.Count == 0)
                throw new TokenizeException(1, 2, "must contain a list of drones");

            var root = new Root(grid, drones);
            return root;
        }

        /// <summary>
        /// Hace un parsing del tamaño de la región
        /// </sumary>
        private static GridItem GetGrid(ref int position, ref List<IToken> tokens)
        {
            // Comprobamos el número de columnas
            var columns = ParserHelper.GetNumber(position++, ref tokens);

            // Comprobamos el espacio
            ParserHelper.CheckSpace(position++, ref tokens);

            // Comprobamos el número de filas
            var rows = ParserHelper.GetNumber(position++, ref tokens);
            
            // Comprobamos el salto de linea
            ParserHelper.CheckNewLine(position++, ref tokens);

            var grid = new GridItem(rows.Value, columns.Value);
            return grid;
        }

        private static List<DronItem> GetDrones(ref int position, ref List<IToken> tokens)
        {
            var drones = new List<DronItem>();

            while (ParserHelper.IsToken(position, ref tokens) && !ParserHelper.IsNewLine(position, ref tokens))
            {
                var dron = GetDron(ref position, ref tokens);
                drones.Add(dron);
            }

            return drones;
        }

        /// <summary>
        /// Obtiene un dron
        /// </summary>
        private static DronItem GetDron(ref int position, ref List<IToken> tokens)
        {
            // Comprobamos la column donde está del dron
            var column = ParserHelper.GetNumber(position++, ref tokens);

            // Comprobamos el espacio
            ParserHelper.CheckSpace(position++, ref tokens);

            // Comprobamos la fila donde está el dron
            var row = ParserHelper.GetNumber(position++, ref tokens);

            // Comprobamos el segundo espacio
            ParserHelper.CheckSpace(position++, ref tokens);

            // Comprobamos la dirección del dron
            var direction = GetDirection(ref position, ref tokens);

            // Comprobamos el salto de linea
            ParserHelper.CheckNewLine(position++, ref tokens);

            List<ActionItem> actions = GetActions(ref position, ref tokens);

            if (actions.Count == 0)
                throw new TokenizeException(1, row.Line + 1, "must contain a list of actions");

            return new DronItem(row.Value, column.Value, direction, actions);
        }

        /// <summary>
        /// Obtiene la lista de acciones del dron
        /// </summary>
        private static List<ActionItem> GetActions(ref int position, ref List<IToken> tokens)
        {
            var actions = new List<ActionItem>();

            while (ParserHelper.IsToken(position, ref tokens) && !ParserHelper.IsNewLine(position, ref tokens))
            {
                var action = GetAction(ref position, ref tokens);
                actions.Add(action);
            }
            position++;

            return actions;
        }

        /// <summary>
        /// Obtiene una dirección
        /// </summary>
        private static DronItem.DirectionType GetDirection(ref int position, ref List<IToken> tokens)
        {
            var direction = ParserHelper.GetLetter(position++, ref tokens);
            if (!"NSWE".Contains(direction.Value))
                throw new TokenizeException(direction.Character, direction.Line, "direction must be 'N', 'S', 'W' or 'E'");

            DronItem.DirectionType type = DronItem.DirectionType.North;
            switch (direction.Value)
            {
                case 'S':
                    type = DronItem.DirectionType.South;
                    break;
                case 'W':
                    type = DronItem.DirectionType.West;
                    break;
                case 'E':
                    type = DronItem.DirectionType.East;
                    break;
            }

            return type;
        }

        /// <summary>
        /// Obtiene una acción
        /// </summary>
        private static ActionItem GetAction(ref int position, ref List<IToken> tokens)
        {
            var move = ParserHelper.GetLetter(position++, ref tokens);
            if (!"LRM".Contains(move.Value))
                throw new TokenizeException(move.Character, move.Line, "movement must be 'L', 'R' or 'M'");

            ActionItem action = null;
            switch (move.Value) 
            {
                case 'M':
                    action = new MoveForwardAction();
                    break;
                case 'L':
                    action = new RotateLeftAction();
                    break;
                case 'R':
                    action = new RotateRightAction();
                    break;
            }

            return action;
        }
    }
}
