namespace ParqueMonserrat.ControlForestal
{
    /// <summary>
    /// Representa el estado actual de un dron
    /// </summary>
    public class Dron
    {
        // Direción hacia donde mira el Dron
        public enum Direction { North, South, East, West }

        public int Row { get; set; }
        public int Column { get; set; }
        public Direction PointAt { get; set; }

        public override string ToString()
        {
            return $"Dron<{Row}, {Column}, {PointAt}>";
        }
    }
}