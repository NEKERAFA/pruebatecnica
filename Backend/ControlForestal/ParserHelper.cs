using ParqueMonserrat.ControlForestal.Lexer;
using System;
using System.Collections.Generic;

namespace ParqueMonserrat.ControlForestal
{
    /// <summary>
    /// Clase que sirve para ayudar al parser
    /// </sumary>
    public static class ParserHelper
    {
        public static bool IsToken(int position, ref List<IToken> tokens)
        {
            return (tokens != null) && (position < tokens.Count) && (tokens[position] != null);
        }

        public static void CheckToken(int position, ref List<IToken> tokens)
        {
            if (!IsToken(position, ref tokens))
            {
                var character = 0;
                var line = 1;
                
                /// Si la lista no está vacia, se coge la posición del último caracter definido
                if (tokens != null && tokens.Count > 0)
                {
                    var last_token = (position < tokens.Count) ? position : tokens.Count;
                    while (last_token > -1 && tokens[last_token] == null)
                        last_token--;

                    if (last_token > -1)
                    {
                        var token = tokens[tokens.Count - 1];
                        character = token.Character;
                        line = token.Line;
                    }
                }

                throw new TokenizeException(character++, line, "unexpected EOF");
            }
        }

        public static bool IsNumber(int position, ref List<IToken> tokens)
        {
            if (!IsToken(position, ref tokens))
                return false;

            var token = tokens[position];
            return token.IsNumber();
        }

        public static void CheckNumber(int position, ref List<IToken> tokens)
        {
            CheckToken(position, ref tokens);

            if (!IsNumber(position, ref tokens))
            {
                var token = tokens[position];
                throw new TokenizeException(token.Character, token.Line, $"expected {Number.TypeToString()}, got {token}");
            }
        }

        public static Number GetNumber(int position, ref List<IToken> tokens)
        {
            CheckNumber(position, ref tokens);
            var token = tokens[position] as Number;
            return token;
        }

        public static bool IsLetter(int position, ref List<IToken> tokens)
        {
            if (!IsToken(position, ref tokens))
                return false;

            var token = tokens[position];
            return token.IsLetter();
        }

        public static void CheckLetter(int position, ref List<IToken> tokens)
        {
            CheckToken(position, ref tokens);

            if (!IsLetter(position, ref tokens))
            {
                var token = tokens[position];
                throw new TokenizeException(token.Character, token.Line, $"expected {Letter.TypeToString()}, got {token}");
            }
        }

        public static Letter GetLetter(int position, ref List<IToken> tokens)
        {
            CheckLetter(position, ref tokens);
            var token = tokens[position] as Letter;
            return token;
        }

        public static bool IsSymbol(int position, ref List<IToken> tokens)
        {
            if (!IsToken(position, ref tokens))
                return false;

            var token = tokens[position];
            return token.IsSymbol();
        }

        public static void CheckSymbol(int position, ref List<IToken> tokens)
        {
            CheckToken(position, ref tokens);

            if (!IsSymbol(position, ref tokens))
            {
                var token = tokens[position];
                throw new TokenizeException(token.Character, token.Line, $"expected {Symbol.TypeToString()}, got {token}");
            }
        }

        public static Symbol GetSymbol(int position, ref List<IToken> tokens)
        {
            CheckSymbol(position, ref tokens);
            var token = tokens[position] as Symbol;
            return token;
        }

        public static bool IsSpace(int position, ref List<IToken> tokens)
        {
            if (!IsToken(position, ref tokens))
                return false;

            var token = tokens[position];
            return token.IsSpace();
        }

        public static void CheckSpace(int position, ref List<IToken> tokens)
        {
            CheckToken(position, ref tokens);

            if (!IsSpace(position, ref tokens))
            {
                var token = tokens[position];
                throw new TokenizeException(token.Character, token.Line, $"expected {Space.TypeToString()}, got {token}");
            }
        }

        public static Space GetSpace(int position, ref List<IToken> tokens)
        {
            CheckSpace(position, ref tokens);
            var token = tokens[position] as Space;
            return token;
        }

        public static bool IsNewLine(int position, ref List<IToken> tokens)
        {
            if (!IsToken(position, ref tokens))
                return false;

            var token = tokens[position];
            return token.IsNewLine();
        }

        public static void CheckNewLine(int position, ref List<IToken> tokens)
        {
            CheckToken(position, ref tokens);

            if (!IsNewLine(position, ref tokens))
            {
                var token = tokens[position];
                throw new TokenizeException(token.Character, token.Line, $"expected {NewLine.TypeToString()}, got {token}");
            }
        }

        public static NewLine GetNewLine(int position, ref List<IToken> tokens)
        {
            CheckNewLine(position, ref tokens);
            var token = tokens[position] as NewLine;
            return token;
        }
    }
}
